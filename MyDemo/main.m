//
//  main.m
//  MyDemo
//
//  Created by jinfu zhang on 2017/8/2.
//  Copyright © 2017年 jinfu zhang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
