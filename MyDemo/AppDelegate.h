//
//  AppDelegate.h
//  MyDemo
//
//  Created by jinfu zhang on 2017/8/2.
//  Copyright © 2017年 jinfu zhang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

